import { Component, OnInit } from "@angular/core";
import { ScrollNavigatorService } from "src/app/services/scroll-navigator.service";
import { DataStoreService } from 'src/app/services/data-store.service';

@Component({
  selector: "app-welcome",
  templateUrl: "./welcome.component.html",
  styleUrls: ["./welcome.component.scss"]
})
export class WelcomeComponent implements OnInit {
  constructor(public scNav: ScrollNavigatorService, public store:DataStoreService) {
    
  }

  ngOnInit() {}
}
