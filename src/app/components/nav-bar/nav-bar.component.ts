import { Component, OnInit, HostListener } from "@angular/core";
import { ScrollNavigatorService } from "src/app/services/scroll-navigator.service";
import $ from "jquery";

@HostListener("window:scroll", ["$event"])
@Component({
  selector: "app-nav-bar",
  templateUrl: "./nav-bar.component.html",
  styleUrls: ["./nav-bar.component.scss"]
})
export class NavBarComponent implements OnInit {
  public posClass = "home";
  constructor(public scNav: ScrollNavigatorService) {}

  ngOnInit() {}

  @HostListener("window:scroll", ["$event"])
  onWindowScroll(e) {
    console.log(window.pageYOffset);
    if (window.pageYOffset > 520) {
      let element = document.getElementById("header");
      element.classList.add("navbar-scrolled");
      this.posClass = "about";
      if (window.pageYOffset > 1200) {
        this.posClass = "map";
      }
      if (window.pageYOffset > 1960) {
        this.posClass = "contact";
      }
      if (window.pageYOffset > 2660) {
        this.posClass = "none";
      }
    } else {
      this.posClass = "home";
      let element = document.getElementById("header");
      element.classList.remove("navbar-scrolled");
    }
  }
}
