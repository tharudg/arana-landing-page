import { Component, OnInit } from "@angular/core";
import { ScrollNavigatorService } from "src/app/services/scroll-navigator.service";
import { ApiCallerService } from "src/app/services/api-caller.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-contact-us",
  templateUrl: "./contact-us.component.html",
  styleUrls: ["./contact-us.component.scss"]
})
export class ContactUsComponent implements OnInit {
  urlTreeIcon = "../../../assets/treeicon.svg";

  messageBody = { name: "", email: "", phone_no: "", message: "" };

  constructor(
    public scNav: ScrollNavigatorService,
    public api: ApiCallerService,
    public toastr: ToastrService
  ) {}

  ngOnInit() {}

  sendMessage() {
    this.api.sendMessage(this.messageBody).subscribe(res => {
      if (res == "OK") {
        this.toastr.success("Message has been sent", "Message sent !", {
          positionClass: "toast-bottom-center"
        });
      } else {
        this.toastr.error("Something went wrong", "Message sent failed!", {
          positionClass: "toast-bottom-center"
        });
      }
    });
  }
}
