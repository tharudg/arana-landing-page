import { Component, OnInit, Input } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { ApiCallerService } from "src/app/services/api-caller.service";
import { DataStoreService } from "src/app/services/data-store.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: "app-claim-tree",
  templateUrl: "./claim-tree.component.html",
  styleUrls: ["./claim-tree.component.scss"]
})
export class ClaimTreeComponent implements OnInit {
  @Input() color: boolean = false;
  claimTree: FormGroup;
  constructor(
    public toastr: ToastrService,
    public api: ApiCallerService,
    public dataService: DataStoreService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.claimTree = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]]
    });
  }
  get f() {
    return this.claimTree.controls;
  }
  send() {
    let data = {
      country_code: this.dataService.locationDetails.country_code2,
      country: this.dataService.locationDetails.country_name,
      email: this.claimTree.get("email").value
    };
    if (!this.claimTree.invalid) {
      this.api.claimTree(data).subscribe(res => {
        if (res == "OK") {
          this.toastr.success(
            `to ${this.claimTree.get("email").value}`,
            "Email sent succesfull!",
            {
              positionClass: "toast-bottom-center"
            }
          );
        } else {
          this.toastr.error(
            "email sent failed check your network",
            "Send Failed !",
            {
              positionClass: "toast-bottom-center"
            }
          );
        }
      });
    } else {
      this.toastr.error(
        "email sent failed due to invalid e-mail !",
        "Invalid email !",
        {
          positionClass: "toast-bottom-center"
        }
      );
    }
  }
}
