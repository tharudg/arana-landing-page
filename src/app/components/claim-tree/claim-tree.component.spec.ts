import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimTreeComponent } from './claim-tree.component';

describe('ClaimTreeComponent', () => {
  let component: ClaimTreeComponent;
  let fixture: ComponentFixture<ClaimTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
