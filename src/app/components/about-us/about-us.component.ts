import { Component, OnInit } from "@angular/core";
import { ScrollNavigatorService } from "src/app/services/scroll-navigator.service";
import { DataStoreService } from "src/app/services/data-store.service";

@Component({
  selector: "app-about-us",
  templateUrl: "./about-us.component.html",
  styleUrls: ["./about-us.component.scss"]
})
export class AboutUsComponent implements OnInit {
  constructor(
    public scNav: ScrollNavigatorService,
    public store: DataStoreService
  ) {}
  images = [];
  ngOnInit() {
    this.images = this.store.corouselImages;
  }
}
