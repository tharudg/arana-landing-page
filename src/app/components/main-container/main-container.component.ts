import { Component, OnInit } from "@angular/core";
import { ScrollNavigatorService } from "src/app/services/scroll-navigator.service";
import { ToastrService } from "ngx-toastr";
import { HttpClient } from "@angular/common/http";
import { VisitorsService } from "src/app/services/visitor.service";
import { DataStoreService } from "src/app/services/data-store.service";
import { ApiCallerService } from "src/app/services/api-caller.service";
import * as am4core from "@amcharts/amcharts4/core";

@Component({
  selector: "app-main-container",
  templateUrl: "./main-container.component.html",
  styleUrls: ["./main-container.component.scss"]
})
export class MainContainerComponent implements OnInit {
  constructor(
    public scNav: ScrollNavigatorService,
    public toastr: ToastrService,
    public visitorsService: VisitorsService,
    public dataService: DataStoreService,
    public api: ApiCallerService
  ) {}

  ngOnInit() {
    // this.api.getTreeCount().subscribe(data => console.log(data));
    this.visitorsService.getIpAddress().subscribe(res => {
      this.visitorsService.getGEOLocation(res["ip"]).subscribe(res => {
        this.dataService.locationDetails = res;
      });
    });
  }
}
