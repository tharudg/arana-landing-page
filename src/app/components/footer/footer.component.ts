import { Component, OnInit } from "@angular/core";
import { ScrollNavigatorService } from "src/app/services/scroll-navigator.service";
import { DataStoreService } from "src/app/services/data-store.service";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent implements OnInit {
  copyright;
  constructor(
    public scNav: ScrollNavigatorService,
    public store: DataStoreService
  ) {}

  ngOnInit() {
    this.copyright = Date.now();
  }
}
