import {
  Component,
  OnInit,
  NgZone,
  AfterViewInit,
  OnDestroy
} from "@angular/core";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { ScrollNavigatorService } from "src/app/services/scroll-navigator.service";
import { ApiCallerService } from "src/app/services/api-caller.service";
import { map } from "@amcharts/amcharts4/.internal/core/utils/Iterator";
import { DataStoreService } from "src/app/services/data-store.service";
import { async } from "@angular/core/testing";

am4core.useTheme(am4themes_animated);

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.scss"]
})
export class MapComponent implements OnInit, AfterViewInit {
  countryDetails = [
    {
      id: "IN",
      name: "India",
      value: 30,
      fill: "green"
    }
  ];
  constructor(
    private zone: NgZone,
    public scNav: ScrollNavigatorService,
    private api: ApiCallerService,
    public dataStore: DataStoreService
  ) {}

  ngOnInit() {}

  ngAfterViewInit() {
    this.api.getMapData().then((data: Array<any>) => {
      this.countryDetails = data;
      this.zone.runOutsideAngular(() => {
        var chart = am4core.create("chartdiv", am4maps.MapChart);
        chart.geodata = am4geodata_worldLow;
        chart.projection = new am4maps.projections.Miller();
        chart.chartContainer.wheelable = false;
        chart.chartContainer.draggable = false;
        chart.draggable = false;
        chart.padding(30, 20, 20, 20);

        chart.seriesContainer.draggable = false;
        chart.seriesContainer.resizable = false;

        var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
        polygonSeries.useGeodata = true;

        var polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.fill = am4core.color("white");
        // polygonTemplate.stroke = am4core.color("#F1EFEE");
        polygonTemplate.strokeWidth = 2;
        polygonSeries.data = this.countryDetails;

        polygonSeries.tooltip.getFillFromObject = false;
        polygonSeries.tooltip.background.fill = am4core.color("#07BEB8");
        polygonSeries.tooltip.background.opacity = 0;

        polygonTemplate.tooltipHTML = `
  
        <div class="ui small card" style="width: 150px; color:black !important; border:solid 1px; border-color:Gray">
          <div class="content">
  
            <div style="font-weight: bolder;font-size: small;" align="center">{name}</div>
  
            <div class="ui comments" style="margin-top: 0;">
              <div class="comment">
                <a class="avatar">
                  <img src="../../../assets/treeicon.svg" />
                </a>
                <div class="content">
                  <div class="text" align="left"  style="margin-left:-6px !important; font-size:large; margin-bottom:0">
                    <a class="author">{value}</a>
                  </div>
                </div>
                  <div class="metadata">
                    Trees
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
  
        `;
        var hs = polygonTemplate.states.create("hover");
        hs.properties.fill = am4core.color("rgb(79, 121, 79)");
        hs.properties.fillOpacity = 0.5;
        polygonSeries.exclude = ["AQ"];

        var filter1 = polygonSeries.filters.push(
          new am4core.DropShadowFilter()
        );
        filter1.color = am4core.color("#07471B");
        filter1.dx = 0;
        filter1.dy = 0;
        filter1.blur = 25;
        filter1.opacity = 0.3;

        polygonTemplate.propertyFields.fill = "fill";
      });
    });
  }
}
