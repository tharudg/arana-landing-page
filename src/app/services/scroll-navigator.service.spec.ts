import { TestBed } from '@angular/core/testing';

import { ScrollNavigatorService } from './scroll-navigator.service';

describe('ScrollNavigatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScrollNavigatorService = TestBed.get(ScrollNavigatorService);
    expect(service).toBeTruthy();
  });
});
