import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ApiCallerService {
  private baseUrl = "http://dev.arana.echonlabs.com/api/";
  private path = {
    emailsent: this.baseUrl + "claim",
    claimedTrees: this.baseUrl + "trees",
    treesByCountries: this.baseUrl + "trees/map",
    sendMessage: this.baseUrl + "contact"
  };

  constructor(private http: HttpClient) {}

  getTreeCount() {
    return this.http.get(this.path.claimedTrees);
  }
  async getMapData() {
    const response = await this.http
      .get(this.path.treesByCountries)
      .toPromise();
    return response;
  }
  claimTree(data: {}) {
    return this.http.post(this.path.emailsent, data);
  }
  sendMessage(data: {}) {
    return this.http.post(this.path.sendMessage, data);
  }
}
