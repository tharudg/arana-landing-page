import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { WelcomeComponent } from "./components/welcome/welcome.component";
import { MapComponent } from "./components/map/map.component";
import { AboutUsComponent } from "./components/about-us/about-us.component";
import { ContactUsComponent } from "./components/contact-us/contact-us.component";
import { NavBarComponent } from "./components/nav-bar/nav-bar.component";
import { MainContainerComponent } from "./components/main-container/main-container.component";
import { SuiModule } from "ng2-semantic-ui";
import { ClaimTreeComponent } from "./components/claim-tree/claim-tree.component";
import { FooterComponent } from "./components/footer/footer.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ToastrModule } from "ngx-toastr";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { VisitorsService } from "./services/visitor.service";

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    MapComponent,
    AboutUsComponent,
    ContactUsComponent,
    NavBarComponent,
    MainContainerComponent,
    ClaimTreeComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SuiModule,
    FormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [VisitorsService],
  bootstrap: [AppComponent]
})
export class AppModule {}
